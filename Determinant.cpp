// SSE Praktikum.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>

int main()
{
	/*
	Matrix:
	1   8 1.5
	0   1 3
	2.1 8 7.7
	*/
	auto t1 = std::chrono::high_resolution_clock::now();

	double matrix[9] = 
	{ 
		1, 8, 1.5, 0, 1, 3, 2.1, 8, 7.7
	};

	double determinant = 0;

	//3x3 --> 3x5
	double calMatrix[15];
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t n = 0; n < 5; n++)
		{
			calMatrix[n + 5 * i] = matrix[i * 3 + (n % 3)];
		}
	}

	for (size_t n = 0; n < 3; n++)
	{
		double diagonal = 1;
		for (size_t i = 0; i < 3; i++)
		{
			diagonal *= calMatrix[i * 5 + i + n];
		}
		determinant += diagonal;
	}

	for (size_t n = 2; n < 5; n++)
	{
		double diagonal = 1;
		for (size_t i = 0; i < 3; i++)
		{
			diagonal *= calMatrix[i * 4 + n];
		}
		determinant -= diagonal;
	}
	
	auto t2 = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

	std::cout << "Duration: " << duration << "\n";
	std::cout << "Determinant: " << determinant << "\n";
	
	/*
	for (size_t i = 0; i < 15; i++)
	{
		std::cout << calMatrix[i] << " ";
	}
	*/

	/*
	for (int i = 0; i < sizeof(matrix[0]) / sizeof(int); i++)
	{
		//subdeterminant
		int subdeterminant = 0;
		for (int n = 0; n < sizeof(matrix[0]) / sizeof(int); n++)
		{
			if (n != i)
			{
				int sign;
				if ()
				{

				}
			}
		}

		int sign;
		if (i % 2 == 0)
		{
			sign = 1;
		}
		else
		{
			sign = -1;
		}

		determinant += sign * matrix[0][i] * subdeterminant;
	}
	*/
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
